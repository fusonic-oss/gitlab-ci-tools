#!/usr/bin/env pwsh

<#
.SYNOPSIS
Cleanup-SQS.ps1
A utility script to remove queues from SQS that do not have any corresponding branch in the git repository.
.PARAMETER AccessToken
The gitlab API access token. This is normally the token of an own API-User created for that project.
.PARAMETER ProjectIds
The gitlab IDs of the projects where the branch names should be taken from.
.PARAMETER Pattern
Regex-pattern of the SQS queues. Any queues that don't match this pattern won't be deleted.
Please go sure to put the branch (slug or name) into the first matching group.
.PARAMETER DryRun
If this switch is set, the script won't actually delete any SQS queues. Only the name of the
queues that would be removed will be printed, but no action is taken on them.
.PARAMETER Exclude
Define a list of branch names for queues that should never be removed. This is for queues that don't have a
branch or just as a safeguard.
.EXAMPLE
Cleanup-SQS.ps1 -DryRun -Exclude "stage","production" -AccessToken "abcde" -ProjectIds 12345 -Pattern "project-(.+)-queue"
#>

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !! If you change anything here, tag it and also update the tags !!
# !! used in cleanup_SQS.yml and cleanup_SQS.md                   !!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

param (
    [Parameter(Mandatory=$true)][string]$AccessToken,
    [Parameter(Mandatory=$true)][int[]]$ProjectIds,
    [Parameter(Mandatory=$true)][string]$Pattern,
    [switch]$DryRun,
    [string[]]$Exclude = @()
)

. ../Shared/Utils.ps1

$ErrorActionPreference = "Stop"

if ($DryRun) {
    Write-Output "Dry run. Changes won't be applied."
    $dryRunPrefix = "(dryRun) "
}

#get current branches in repository
$branches = (Get-Branches -AccessToken $AccessToken -ProjectIds $ProjectIds).name
if ($branches.Count -eq 0) {
    Write-Error "No branches available"
    exit 1
}

Write-Output "Found $($branches.Count) branches:" $branches

$branchSlugs = $branches.foreach{ Get-Slug $PSItem }

# Get queue urls
$queues = (aws sqs list-queues | jq -r ".QueueUrls[]")
if ($LastExitCode -ne 0) { throw "aws sqs exited with code $LastExitCode." }

foreach ($queue in $queues) {
    if (!($queue -match $Pattern)) { continue }
    
    if ($Matches.count -lt 2) {
        Write-Error "Missing group for branch."
        exit 1
    }

    if (!$branches.Contains($Matches.1) -and !$branchSlugs.Contains($Matches.1) -and !$Exclude.Contains($Matches.1)) {
        Write-Output "$($dryRunPrefix)Removing queue $queue"

        if (!$DryRun) {
            aws sqs delete-queue --queue-url "$queue"
            if ($LastExitCode -ne 0) { throw "aws sqs exited with code $LastExitCode." }
        }
    }
}

Write-Output "Done"