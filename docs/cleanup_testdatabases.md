# Cleanup test databases

This tool is designed as a nightly task. It gets all branches of a project and deletes all test databases, that are not in use by one of the branches.

The test databases are expected to be in a format `$PREFIX$CI_COMMIT_REF_SLUG`, for example `ProjectTest_$CI_COMMIT_REF_SLUG`.

## Requirements

The script requires a project access token with `api` access.

Put that API key into the CI-Variables of the project. `GITLAB_APIKEY` is the variable that is used by default, but it can be overwritten (see [Usage](#usage)).

The project ID is taken directly from the gitlab variable `CI_PROJECT_ID`.

It also requires the test database prefix and the connection string.

## Usage

**1. Include the gitlab-ci template in your `.gitlab-ci.yml`:**  

```yml
include:
  - project: 'fusonic/devops/images/gitlab-ci-tools'
    file: 'cleanup_testdatabases.yml'
    ref: 3.1
```

Please check the repository for the latest tag to be used in `ref`. If you're brave you can skip this setting to always get the latest version, but this isn't recommended as parameters and the behaviour of the scripts may change.

**2. Get the API key**  
As outlined in [Requirements](#requirements) you need an access token with `api` access. The token can be created in the gitlab project under `Settings -> Access Tokens`. Put the token in the CI-Variable `GITLAB_APIKEY`.

**3. Configure the cleanup step**  
Configure the step like outlined below. You can skip the optional lines.

```yml
cleanup:testdatabases:
  stage: cleanup #Optional. The default stage is cleanup. You can change this if you want to have it in another step.
  variables:
    TESTDB_PREFIX: ProjectTest_ #Required. The prefix for the test databases
    CONNECTION_STRING: Host=localhost;Database=postgres;User=hi;Password=there; #Required. The connection string to the database.
    GITLAB_APIKEY: $GITLAB_NIGHTLY_APIKEY #Optional override for the API key, if you don't want to name it GITLAB_APIKEY in your CI variables.
    CLEANUP_PARAMETERS: #Optional. Additional parameters for the script. See the parameter description below for details.
```

**4. Enable it**  
The step is only active if one of the variables `CLEANUP_TESTDATABASES` or `CLEANUP_ALL` are set to `true`. Go to your nightly schedule and set one of those variables.

## Parameters

| | |
|-|-|
|`-DryRun` | Don't drop the databases. Only outputs the command that would get executed. |
|`-Exclude <List>` | Additonal databases to exclude from dropping. <br> Example: `-Prefix ProjectTest_ -Exclude "Template","Test"` ignores databases starting with `ProjectTest_Template` and `ProjectTest_Test` (plus all CI_COMMIT_REF_SLUG of the branches) |
| | |
