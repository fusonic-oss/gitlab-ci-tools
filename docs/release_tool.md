# Release tool
The release tool combines the [Gitlab Release CLI](https://gitlab.com/gitlab-org/release-cli) with scripts for
automated changelog generation and commit + merge request title linting in one Docker image for easy use in the CI/CD
pipeline.

There are two jobs included:
1. `release:lint-merge-request`: This job checks if the merge request title is following the conventions based on the
   configuration. See the [commitlint config](../Release-Tool/release-config/commitlint.config.js) for the rules
   The `LINT_CONFIG` and `CHANGELOG_CONFIG` variables can be overridden to provide a custom configuration.
   By default, this job will run on all merge request pipelines which are **not** prefixed by (`draft:` or `wip:`).
2. `.release-script`: This is the base job doing the actual release. By default, it uses this
   [release configuration](../Release-Tool/release-config/config.js). In the extended job you can set the
   `CHANGELOG_CONFIG` variable to override it. See the [Usage](#Usage) section on how to configure the job.

The release job will generate a release in Gitlab and automatically fill out the description based on the changes
since the last release. The description will contain categorized commits and reference to the closed issues.

### Gitlab project configuration
The Gitlab workflow should be based on fast-forward merges. In order to make the project compatible with
the automated release notes the following needs to be configured in the Gitlab project:

1. Go to `Settings > General > Merge requests`.
2. Make sure `Fast-forward merge` is checked.
3. Edit the `Squash commit message template` with the following:

```
%{title}

%{issues}
```

### Commits

#### Default behaviour
1. Within a branch there are no requirements for the commit messages.
2. For the merge requests there are some requirements (they are automatically linted by the `release:lint-merge-request` job)
    1. The `title` must be of [this format](https://www.conventionalcommits.org/en/v1.0.0/#summary):
       `<type>(<scope>): <short summary>`
        1. The possible `type` [values](../Release-Tool/release-config/types.js) are:
           1. feature
           2. revert
           3. fix
           4. docs
           5. cs
           6. ci
           7. infra
           8. refactor
           9. perf
           10. test
           11. misc
           12. security
        2. The `scope` is optional and should be a one-word describing what has changed. For example:
           `controller`, `security` or `orm`. You could also reference issues here (beware that they must also be mentioned 
           in the merge request description if you want them to close after merging)
        4. The `short summary` describes the changes in short.
        5. Everything in the title must be lowercase.
    2. To indicate a breaking change, you can also add a `!`, like this: `<type>(<scope>)!: <short summary>`
    3. In the `description` you should reference all the issues that will be closed by the merge request:
       `Closes #1, #2 and #3`

Here are some example titles:
* `refactor(orm): refactor orm database logic`
* `docs: update the readme installation instructions`
* `revert: this reverts some commit`
* `feature(#44|#43)!: two features with a breaking change `
* `security(parent-project#14): security fix for an issue in the parent project`

#### Optional presets
The `default` preset (no extra configuration required) is recommended. However, if your projects doesn't work with
the default configuration you can use a different preset. Presets included are:

* `classic`: Allows merge request titles like: `[feature][#23] Example Feature`, `[fix] Example bug fix`

In the pipeline jobs you can set the `FORMAT_PRESET` variable to configure this.

*If your project requires a different configuration preset you can create a merge request to get it included.
In [Release-Tool/release-config/presets](../Release-Tool/release-config/presets) directory you can add a new
preset: a Javascript file exporting an object with `linting` ([commitlint](https://github.com/conventional-changelog/commitlint))
and `config` ([conventional-changelog](https://github.com/conventional-changelog/conventional-changelog-config-spec)].
Depending on the configuration; a modification to the `config.js` file might also be necessary.*

#### Custom release notes template
On the release job you can set the `MAIN_TEMPLATE` variable to override the default template for the release notes.
Beware that the template consists for partials for rendering the commits and the header. If you need a completely
different template, consider a complete different configuration set via `CHANGELOG_CONFIG`.

#### Slack webhook
If you want to post the release notes directly to a Slack channel you can configure a webhook and set the url
with `SLACK_WEBHOOK_URL`. Check the [Slack website](https://api.slack.com/messaging/webhooks) on how to
create a webhook.

You can override the default Slack message by setting the `SLACK_TEMPLATE` variable.
See the default [slack.json](../Release-Tool/release-config/slack.json).
*Hint: Slack has a tool called "Block Kit Builder" to generate this json.*

## Usage
**1. Include the gitlab-ci template in your `.gitlab-ci.yml`:**  

```yml
include:
  - project: 'fusonic/devops/images/gitlab-ci-tools'
    file: 'release_tool.yml'
    ref: 3.3
```

Please check the repository for the latest tag to be used in `ref`.
If you're brave you can skip this setting to always get the latest version, but this isn't recommended as parameters and the behaviour of the scripts may change.

**2. Add the `release` stage in your `.gitlab-ci.yml`:**

```yml
stages:
    # ... other stages
    - release
```

**3. Configure the release step**  

```yml
release:create:
    extends: .release-script
    
#    Optionally a custom configuration can be configured
#    variables:
#        CHANGELOG_CONFIG: /custom-changelog-config.js
#        MAIN_TEMPLATE: './custom-template.hbs' # Custom "handlebars" template for the release notes
#        SLACK_WEBHOOK_URL: "https://hooks.slack.com/xxx"
#        SLACK_TEMPLATE: ./custom-slack-message.json # Custom configuration for the Slack message

    # You must configure a before_script and set the RELEASE_VERSION there.
    before_script:
        # You can get your release version from whatever source you like, in this example there is a file containing
        # the latest release version
        - export RELEASE_VERSION=$(cat ./.version)
    rules:
        # By default the script is never run. You must configure a rule here if you want to run the release script.
        - if: $CI_COMMIT_REF_SLUG == "production"
          when: manual
```
