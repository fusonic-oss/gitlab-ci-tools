# Cleanup environments

Gitlab does not always stop an enviroment when a merge request is closed or a branch is deleted. This may be due to various reasons. It may happen for example when a maintainer merges a branch while the pipeline is still running.

In the end this leads to zombie instances on EC2, unused databases and so on.

This tool is designed as a nightly task. It runs through all environments of a project. Those who do not have a branch in the repository will get stopped and deleted.  
Calling an explicit stop action before deleting the environments will trigger the `on_stop` action defined in the `.gitlab-ci.yml`. The infrastructure should get cleaned up by this and the zombies should vanish. Yay!

## Environment naming

The script assumes that the environments are named in the following way:

```
[category/]$CI_COMMIT_REF_SLUG
```

`$CI_COMMIT_REF_SLUG` is defined as the branch name, lowercased, shortened to 63 bytes, and with everything except 0-9 and a-z replaced with -. No leading / trailing -.

For example, a branch `#123-feature` can have the environment names

* 123-feature
* reports/123-feature
* stuff/123-feature

Environments like the following will not be detected as belonging to this branch, thus would be deleted by the script:

* #123-feature
* reports/123-feature/foo

## Requirements

The script requires a project access token with `api` access.

Put that API key into the CI-Variables of the project. `GITLAB_APIKEY` is the variable that is used by default, but it can be overwritten (see [Usage](#usage)).

The project ID is taken directly from the gitlab variable `CI_PROJECT_ID`.

## Usage

**1. Include the gitlab-ci template in your `.gitlab-ci.yml`:**  

```yml
include:
  - project: 'fusonic/devops/images/gitlab-ci-tools'
    file: 'cleanup_environments.yml'
    ref: 3.1
```

Please check the repository for the latest tag to be used in `ref`. If you're brave you can skip this setting to always get the latest version, but this isn't recommended as parameters and the behaviour of the scripts may change.

**2. Get the API key**  
As outlined in [Requirements](#requirements) you need an access token with `api` access. The token can be created in the gitlab project under `Settings -> Access Tokens`. Put the token in the CI-Variable `GITLAB_APIKEY`.

**3. Configure the cleanup step**  
Override settings if your configuration is different from the default outlined below. If you're fine with the default, skip this step. No extra configuration is required - the `include` in your `.gitlab-ci.yml` is enough.

```yml
cleanup:environments:
  stage: cleanup #The default stage is cleanup. You can change this if you want to have it in another step.
  variables:
    GITLAB_APIKEY: $GITLAB_NIGHTLY_APIKEY #Optional override for the API key, if you don't want to name it GITLAB_APIKEY in your CI variables.
    CLEANUP_PARAMETERS: #Additional parameters for the script. See the parameter description below for details.
```

**4. Enable it**  
The step is only active if one of the variables `CLEANUP_ENVIRONMENTS` or `CLEANUP_ALL` are set to `true`. Go to your nightly schedule and set one of those variables.

## Parameters

| | |
|-|-|
|`-DryRun` | If this switch is set, the script won't actually stop and delete environments. Only the ID/Name of the environments that would be removed will be printed, but no action is taken on them. |
|`-Exclude <List>` | Define a list of environments that should never be stopped. This is for environments that don't have a branch or it just can be used as a safeguard. <br>Example: `-Exclude "translate","whatever"`<br>Note: main, master, stage and production environments will never be deleted, even if there's a branch missing for whatever reason. |
|`-ForceStop` | Force stop environments. Only use this if some of your environments are stuck on `stopping`. When `-ForceStop` is set, the `on_stop` actions won't be executed. |
| | |
