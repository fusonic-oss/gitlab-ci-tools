# .NET test execution

The include `dotnet_test.yml` provides two jobs for executing .NET tests. Both jobs are hidden (prefixed with a dot `.`), so you need to extend one of them and provide the required variables in order to execute the desired test job. 

The jobs get started in the stage `test`. They start immediatly, unless configured otherwise, as `dependencies` and `needs` are configured as empty `[]`.

The tests also create an environment `review/dotnet-test/$CI_COMMIT_REF_SLUG` with the URL pointing to the code coverage HTML report.

Feel free to overwrite the default settings for `stage`, `dependencies`, `needs` and `environment` as needed.

## Job `.dotnet-test`

This job executes the tests of a solution and generates various reports:
- Code coverage HTML report
  - This can be used to upload to an S3 or just be displayed via the "View app"-button in the `dotnet-test` environment
- Code coverage cobertura report
  - This one is used for the coverage info when comparing files in a merge request
- Code coverage percentage
  - This one is displayed in the merge request overview
- Test results in JUnit format
  - This one is used for the test results in the merge request overview, where it tells you how many and which tests failed or succeeded.

All parameters required for gitlab picking the reports up are already set properly.

### Parameters

The following parameters are supported for the `variables`:

| | |
|-|-|
| `SLN_PATH` | Required. The path to the solution that should be tested. |
| `COVERAGE_EXCLUDE` | Optional. Usually you want to have some files, like generated code, being excluded from the code coverage. You can define filters here. The syntax supports wildcards and basically is `[Assembly1]Namespace1,[Assembly2]Namespace2.*`. Syntax details can be found in [the documentation of coverlet](https://github.com/coverlet-coverage/coverlet/blob/master/Documentation/MSBuildIntegration.md#excluding-from-coverage) |
| `BACKEND_SRC_DIR` | Defaults to `src/backend`. This is the base directory where the backend source lies in. |
| `REPORT_OUTPUT_DIR` | Defaults to `reports/dotnetcoverage`. This is the base directory where the dotnet coverage report should be copied to. |
| | |

### Example
```yml
include:
  - project: 'fusonic/devops/images/gitlab-ci-tools'
    file: 'dotnet_test.yml'
    ref: 3.1

dotnet:
  extends: .dotnet-test
  variables:
    SLN_PATH: src/backend/Fusonic.Project.sln
    COVERAGE_EXCLUDE: "[*]Fusonic.Project.Api.Data.Migrations.*"
```

### Extensibility

For this job you can use `before_script` and `after_script` if you need to execute some of your own steps. That's not the case for `.dotnet-db-test`

## Job `.dotnet-db-test`

This job is based on `.dotnet-test`, so it brings all features described above. Additionally it supports (PostgreSQL) test databases. It uses `before_script` to create a test database template and `after_script` to clean up the remaining test databases of the run.

It utilizes `pgtestutil` and the `TemplateCreator` from our test framework in [Fusonic.Extensions.UnitTests](https://github.com/fusonic/dotnet-extensions).

### Parameters

In addition to the parameters defined for `.dotnet-test`, `.dotnet-db-test` supports the following `variables`:

| | |
|-|-|
| `TEMPLATE_CREATOR_ASM_PATH` | Required. The path to the assembly that contains the implementation of the [ITestDbTemplateCreator](https://github.com/fusonic/dotnet-extensions/blob/master/src/UnitTests.Adapters.EntityFrameworkCore/src/ITestDbTemplateCreator.cs). It is used to create the test database template. |
| `DB_HOST` | Required. The address of the database. |
| `DB_USER` | Required. The user (usually the application user, not the postgres user) that should be used to connect to the database. The user must have permissions to create a database. |
| `DB_PASSWORD` | Required. The password for the `DB_USER`. |
| | |

### Example
```yml
include:
  - project: 'fusonic/devops/images/gitlab-ci-tools'
    file: 'dotnet_test.yml'
    ref: 3.1

dotnet:
  extends: .dotnet-db-test
  variables:
    SLN_PATH: src/backend/Fusonic.Project.sln
    COVERAGE_EXCLUDE: "[*]Fusonic.Project.Api.Data.Migrations.*"
    TEMPLATE_CREATOR_ASM_PATH: src/backend/Fusonic.Project.Api.Tests/bin/Debug/net5.0/Fusonic.Project.Api.Tests.dll
    DB_HOST: ${DB_HOST}
    DB_USER: ${DB_APP_USER}
    DB_PASSWORD: ${DB_APP_PASSWORD}
```