# Cleanup AWS SNS subscriptions

This tool is designed as a nightly task. It gets all branches of a project and deletes all SNS subscriptions with endpoints that are not in use by one of the branches.

You can specify the pattern for the endpoint ARN (=SQS queue ARN) that should be matched on cleanup.

## Requirements

The script requires a project access token with `api` access.

Put that API key into the CI-Variables of the project. `GITLAB_APIKEY` is the variable that is used by default, but it can be overwritten (see [Usage](#usage)).

It also requires the pattern to match the subscription endpoints that should be deleted (`PATTERN`), as well as the environment variables required by the aws cli for access.

## Usage

**1. Include the gitlab-ci template in your `.gitlab-ci.yml`:**  

```yml
include:
  - project: 'fusonic/devops/images/gitlab-ci-tools'
    file: 'cleanup_aws_sns_subscription.yml'
    ref: 3.1
```

Please check the repository for the latest tag to be used in `ref`. If you're brave you can skip this setting to always get the latest version, but this isn't recommended as parameters and the behaviour of the scripts may change.

**2. Get the API key**  
As outlined in [Requirements](#requirements) you need an access token with `api` access. The token can be created in the gitlab project under `Settings -> Access Tokens`. Put the token in the CI-Variable `GITLAB_APIKEY`.

**3. Configure the cleanup step**  
Configure the step like outlined below. You can skip the optional lines.

```yml
cleanup:aws:sns-subscriptions:
  stage: cleanup #Optional. The default stage is cleanup. You can change this if you want to have it in another step.
  variables:
    PATTERN: "project-(.+)-queue" #Required. Regex-pattern of the SNS subscription endpoints. Subscriptions with endpoints that don't match this pattern won't be deleted. Requires to have the branch name or branch slug to be in the first match group.
    GITLAB_APIKEY: $GITLAB_NIGHTLY_APIKEY #Optional override for the API key, if you don't want to name it GITLAB_APIKEY in your CI variables.
    CLEANUP_PARAMETERS: #Optional. Additional parameters for the script. See the parameter description below for details.
    PROJECT_IDS: 123,321 #Optional. Comma-separated list of project IDs to get the branches from. Defaults to CI_PROJECT_ID.
```

**4. Enable it**  
The step is only active if one of the variables `CLEANUP_SNS` or `CLEANUP_ALL` are set to `true`. Go to your nightly schedule and set one of those variables.

## Parameters

| | |
|-|-|
|`-DryRun` | If this switch is set, the script won't actually delete any SNS subscriptions. Only the arn of the subscriptions that would be removed will be printed, but no action is taken on them. |
|`-Exclude <List>` | Define a list of branch names for subscription endpoints that should never be removed. This is for subscription endpoints that don't have a branch or just as a safeguard. Example: `"stage","production"` |
| | |
