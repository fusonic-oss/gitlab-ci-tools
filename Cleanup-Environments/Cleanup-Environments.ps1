#!/usr/bin/env pwsh

<#
.SYNOPSIS
Cleanup-Environments.ps1
A utility script to remove environments from a gitlab project that do not have any corresponding
branch in the repository.
.PARAMETER AccessToken
The gitlab API access token. This is normally the token of an own API-User created for that project.
The user must have maintainer access!
.PARAMETER ProjectId
The gitlab ID of the project that should be cleaned.
.PARAMETER DryRun
If this switch is set, the script won't actually stop and delete environments. Only the ID/Name of the
environments that would be removed will be printed, but no action is taken on them.
.PARAMETER ForceStop
Force stop environments. Only use this if some of your environments are stuck on `stopping`. When `-ForceStop` is set, 
the `on_stop` actions won't be executed.
.PARAMETER Exclude
Define a list of environments that should never be stopped. This is for environments that don't have a
branch or just as a safeguard.
Note: main, master, stage and production environments will never be deleted, even if there's a branch missing
for whatever reason.
.EXAMPLE
Cleanup-Environments.ps1 -AccessToken Abcd234fgh!jklmnop532 -ProjectId 1234567 -Exclude "translate","whatever" -DryRun
#>

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !! If you change anything here, tag it and also update the tags !!
# !! used in cleanup_environments.yml and cleanup_environments.md !!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

param (
    [Parameter(Mandatory=$true)][string]$AccessToken,
    [Parameter(Mandatory=$true)][int]$ProjectId,
    [switch]$DryRun,
    [switch]$ForceStop,
    [string[]]$Exclude
)

. ../Shared/Utils.ps1

$ErrorActionPreference = "Stop"

$projectUri = "https://gitlab.com/api/v4/projects/$ProjectId"
$headers = @{ "Private-Token" = $accessToken }

if ($DryRun)
{
    Write-Output "Dry run. Changes won't be applied."
    $dryRunPrefix = "(dryRun) "
}

#get current branches in repository
$branches = (Get-Branches -AccessToken $AccessToken -ProjectIds $ProjectId).name
if ($branches.Count -eq 0)
{
    Write-Error "No branches available"
    exit 1
}

Write-Output "Found $($branches.Count) branches:" $branches

#Slug the branch names and the exclude list
$branches = $branches.foreach{Get-Slug $PSItem}
$Exclude = $Exclude.foreach{Get-Slug $PSItem}

#Get the environments. Gitlab limits results to 20 environments but allows paging
$page = 1
Write-Output `n "Looking for unused environments..."
do
{
    $environments = Invoke-Restmethod -Uri "$projectUri/environments?page=$page" -Headers $headers -Method Get
    $deletedEnvironment = $false

    foreach ($environment in $environments)
    {
        #an environment may be like review/<branch-name>. Split by / and use the last part as branch name
        $parts = $environment.name.Split('/')
        $envName = $parts[$parts.Count - 1]
    
        #never ever delete those environments. They should be in the branch list, but we use this as an additional safeguard
        if ($envName -eq "main" -or $envName -eq "master" -or $envName -eq "stage" -or $envName -eq "production" -or $Exclude.Contains($envName))
        {
            continue
        }
    
        #no branch with this environment? Stop it
        if (!$branches.Contains($envName))
        {
            $id = $environment.id
            $force = ($ForceStop -and $environment.state -eq "stopping")
            
            Write-Output "$($dryRunPrefix)Removing environment $id $($environment.name); Force=$force; State=$($environment.state);"

            if (!$DryRun)
            {
                $queryParams = "force=$force".ToLowerInvariant();
                
                Write-Output "   - Stop; Current state=$($environment.state)"
                Invoke-RestMethod -Uri "$projectUri/environments/$id/stop?$queryParams" -Headers $headers -Method Post | Out-Null
                
                $stoppedEnv = Invoke-Restmethod -Uri "$projectUri/environments/$id" -Headers $headers -Method Get
                Write-Output "   - Delete; Current state=$($stoppedEnv.state)"
                Invoke-RestMethod -Uri "$projectUri/environments/$id" -Headers $headers -Method Delete | Out-Null
                
                $deletedEnvironment = $true
            }
        }
    }

    if (!$deletedEnvironment)
    {
        $page++
    }
} while($environments.Count -gt 0)

Write-Output "Done"
