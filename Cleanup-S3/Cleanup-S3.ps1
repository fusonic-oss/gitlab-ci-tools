#!/usr/bin/env pwsh

<#
.SYNOPSIS
Cleanup-S3.ps1
A utility script to remove folders from an S3 bucket that do not have any corresponding branch in the git repository.
.PARAMETER AccessToken
The gitlab API access token. This is normally the token of an own API-User created for that project.
.PARAMETER ProjectId
The gitlab ID of the project that should be cleaned.
.PARAMETER Pattern
Regex-pattern of the S3 keys. Unmatched keys won't be deleted. Requires to have the branch name or branch slug to be in the first match group.
.PARAMETER Bucket
Name of the S3 bucket.
.PARAMETER DryRun
If this switch is set, the script won't actually delete anything in S3. Only the name of the
keys that would be removed will be printed, but no action is taken on them.
.PARAMETER Exclude
Define a list of branch names for keys that should never be removed. This is for keys that don't have a
branch or just as a safeguard.
.EXAMPLE
Cleanup-S3.ps1 -DryRun -Exclude "stage","production" -AccessToken "abcde" -ProjectId 12345 -Bucket "project-review-bucket" -Pattern "^review-(.+)/"
#>

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !! If you change anything here, tag it and also update the tags !!
# !! used in cleanup_s3.yml and cleanup_s3.md                     !!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

param (
    [Parameter(Mandatory = $true)][string]$AccessToken,
    [Parameter(Mandatory = $true)][int]$ProjectId,
    [Parameter(Mandatory = $true)][string]$Pattern,
    [Parameter(Mandatory = $true)][string]$Bucket,
    [switch]$DryRun,
    [string[]]$Exclude = @()
)

. ../Shared/Utils.ps1

$ErrorActionPreference = "Stop"

if ($DryRun) {
    Write-Output "Dry run. Changes won't be applied."
}

#get current branches in repository
$branches = (Get-Branches -AccessToken $AccessToken -ProjectIds $ProjectId).name
if ($branches.Count -eq 0) {
    Write-Error "No branches available"
    exit 1
}

Write-Output "Found $($branches.Count) branches:" $branches

$branchSlugs = $branches.foreach{ Get-Slug $PSItem }

# Get S3 keys
$keys = (aws s3 ls s3://$Bucket).foreach{ $PSItem.split(' ')[-1] }
if ($LastExitCode -ne 0) { throw "aws s3 exited with code $LastExitCode." }

foreach ($key in $keys) {
    if (!($key -match $Pattern)) { continue }
    
    if ($Matches.count -lt 2) {
        Write-Error "Missing group for branch."
        exit 1
    }
    
    if (!$branches.Contains($Matches.1) -and !$branchSlugs.Contains($Matches.1) -and !$Exclude.Contains($Matches.1)) {
        Write-Output "Removing $key"

        $dryRunParam = "";
        if ($DryRun) {
            $dryRunParam = "--dryrun"
        }

        aws s3 rm "s3://$Bucket/$key" --recursive $dryRunParam
        if ($LastExitCode -ne 0) { throw "aws s3 exited with code $LastExitCode." }
    }
}

Write-Output "Done"