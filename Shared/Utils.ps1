function Get-Branches (
    [Parameter(Mandatory = $true)][string]$AccessToken,
    [Parameter(Mandatory = $true)][int[]]$ProjectIds
) {

    $headers = @{ "Private-Token" = $AccessToken }
    $allBranches = @()

    foreach($projectId in $ProjectIds) {
        #Get the branches. Gitlab limits results to 20 branches but allows paging
        $projectUri = "https://gitlab.com/api/v4/projects/$projectId"
        $page = 1
        do {
            $branches = Invoke-Restmethod -Uri "$projectUri/repository/branches?page=$page" -Headers $headers -Method Get
            $allBranches += $branches
            $page++
        } while ($branches.Count -gt 0)
    }

    return $allBranches
}

function Get-Slug([string]$str) {
    #apply the same logic as for CI_COMMIT_REF_SLUG: lowercased, shortened to 63 bytes, and with everything except 0-9 and a-z replaced with -. No leading / trailing -.
    $str = ($str.ToLower() -replace "((?![0-9a-z]).)", "-").Trim("-")
    $str = $str.Substring(0, [System.Math]::Min(63, $str.Length))
    return $str
}