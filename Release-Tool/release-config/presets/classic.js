'use strict'

const baseConfig = require('@commitlint/config-conventional')
const types = require('../default-types')

/**
 * Preset for commits with the following structure:
 *
 * `[feature][#23] Example feature`
 * `[fix] Example feature`
 *
 */
module.exports = {
    linting: {
        ...baseConfig,
        rules: {
            ...baseConfig.rules,
            'type-enum': [
                2,
                'always',
                types.map(typeConfig => typeConfig.type)
            ],
            'subject-case': [
                2,
                'never',
                ['start-case', 'pascal-case', 'upper-case'],
            ],
        },
    },
    config: {
        types,
        parserOpts: {
            headerPattern: /^\[(.*?)](?:\[(.*)])?!? (.*)$/,
            breakingHeaderPattern: /^\[(.*?)](?:\[(.*)])?! (.*)$/,
            headerCorrespondence: [
                'type',
                'scope',
                'subject'
            ],
            referenceActions: [
                "close",
                "closes",
                "closed",
                "closing",
                "fix",
                "fixes",
                "fixed",
                "fixing"
            ],
        }
    }
};

