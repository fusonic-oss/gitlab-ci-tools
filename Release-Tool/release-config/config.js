'use strict'

const preset = process.env.FORMAT_PRESET || 'default'
const presetConfig = require(`./presets/${preset}`).config;
const writerOpts = require('./writer-opts')
const parserOpts = require('conventional-changelog-conventionalcommits/parser-opts')
const recommendedBumpOpts = require('conventional-changelog-conventionalcommits/conventional-recommended-bump')

module.exports = (callback) => {
    const extendedConfig = {
        ...presetConfig,
        mainTemplate: process.env.MAIN_TEMPLATE
    };

    const extendedParserOpts = {
        ...parserOpts(extendedConfig),
        ...presetConfig?.parserOpts
    };

    const extendedWriterOpts = async () => {
        return writerOpts(extendedConfig)
    }

    const conventionalChangelog = async () => {
        return {
            parserOpts: extendedParserOpts,
            writerOpts: await writerOpts(extendedConfig)
        }
    }

    Promise.all([
        conventionalChangelog(),
        recommendedBumpOpts(),
        extendedWriterOpts()
    ]).then(([conventionalChangelog, recommendedBumpOpts, writerOpts]) => {
        callback(null, {
            gitRawCommitsOpts: {noMerges: null},
            conventionalChangelog,
            parserOpts: extendedParserOpts,
            recommendedBumpOpts,
            writerOpts
        })
    })
}
