'use strict'

const addBangNotes = require('conventional-changelog-conventionalcommits/add-bang-notes')
const compareFunc = require('compare-func')
const resolve = require('path').resolve
const releaseAsRe = /release-as:\s*\w*@?([0-9]+\.[0-9]+\.[0-9a-z]+(-[0-9a-z.]+)?)\s*/i
const fs = require('fs').promises;

/**
 * Handlebar partials for various property substitutions based on commit context.
 */
const owner = '{{#if this.owner}}{{~this.owner}}{{else}}{{~@root.owner}}{{/if}}'
const host = '{{~@root.host}}'
const repository = '{{#if this.repository}}{{~this.repository}}{{else}}{{~@root.repository}}{{/if}}'

/**
 * This configuration is based on the `conventional-changelog-conventionalcommits` preset.
 * (https://www.npmjs.com/package/conventional-changelog-conventionalcommits)
 *
 * Added features compared to the original are:
 *   - Allow overriding of the main template.
 *   - Group issue references by "action" to allow display of grouping per action.
 */
module.exports = async function (config) {
    config = defaultConfig(config)
    const commitUrlFormat = expandTemplate(config.commitUrlFormat, {
        host,
        owner,
        repository
    })
    const compareUrlFormat = expandTemplate(config.compareUrlFormat, {
        host,
        owner,
        repository
    })
    const issueUrlFormat = expandTemplate(config.issueUrlFormat, {
        host,
        owner,
        repository,
        id: '{{this.issue}}',
        prefix: '{{this.prefix}}'
    })

    // Customization: included templates are used from the original package.
    //   The main template is overridden with a new default.
    const templateDirectory = resolve(require.resolve('conventional-changelog-conventionalcommits'), '..', 'templates');
    const mainTemplate = config.template ? resolve(config.mainTemplate) : resolve(templateDirectory, './template.hbs');

    const [template, header, commit, footer] = await Promise.all([
        fs.readFile(mainTemplate, 'utf-8'),
        fs.readFile(resolve(templateDirectory, './header.hbs'), 'utf-8'),
        fs.readFile(resolve(__dirname, './templates/commit.hbs'), 'utf-8'),
        fs.readFile(resolve(templateDirectory, './footer.hbs'), 'utf-8')
    ])

    const writerOpts = getWriterOpts(config)

    writerOpts.mainTemplate = template
    writerOpts.headerPartial = header
        .replace(/{{compareUrlFormat}}/g, compareUrlFormat)

    writerOpts.commitPartial = commit
        .replace(/{{commitUrlFormat}}/g, commitUrlFormat)
        .replace(/{{issueUrlFormat}}/g, issueUrlFormat)

    writerOpts.footerPartial = footer

    return writerOpts
}


function getWriterOpts(config) {
    config = defaultConfig(config)

    return {
        transform: (commit, context) => {
            let discard = true
            const entry = findTypeEntry(config.types, commit)

            // adds additional breaking change notes
            // for the special case, test(system)!: hello world, where there is
            // a '!' but no 'BREAKING CHANGE' in body:
            addBangNotes(commit)

            // Add an entry in the CHANGELOG if special Release-As footer
            // is used:
            if (
                (commit.footer && releaseAsRe.test(commit.footer)) ||
                (commit.body && releaseAsRe.test(commit.body))) {
                discard = false
            }

            commit.notes.forEach(note => {
                note.title = 'BREAKING CHANGES'
                discard = false
            })

            // breaking changes attached to any type are still displayed.
            if (discard && (entry === undefined || entry.hidden)) {
                return
            }

            if (entry) commit.type = entry.section

            if (commit.scope === '*') {
                commit.scope = ''
            }

            if (typeof commit.hash === 'string') {
                commit.shortHash = commit.hash.substring(0, 7)
            }

            if (typeof commit.subject === 'string') {
                config.issuePrefixes.join('|')

                /**
                 * Callback to replace issue urls in the template.
                 */
                const replaceIssueUrls = (_, prefix, issue) => {
                    const url = expandTemplate(config.issueUrlFormat, {
                        host: context.host,
                        owner: context.owner,
                        repository: context.repository,
                        id: issue,
                        prefix: prefix
                    })
                    return `[${prefix}${issue}](${url})`
                }

                /**
                 * Callback to replace user urls in the template.
                 */
                const replaceUserUrls = (_, user) => {
                    if (user.includes('/')) {
                        return `@${user}`
                    }

                    const usernameUrl = expandTemplate(config.userUrlFormat, {
                        host: context.host,
                        owner: context.owner,
                        repository: context.repository,
                        user: user
                    })

                    return `[@${user}](${usernameUrl})`
                }

                const issueRegEx = '(' + config.issuePrefixes.join('|') + ')' + '([0-9]+)'

                commit.subject = commit.subject.replace(new RegExp(issueRegEx, 'g'), replaceIssueUrls)
                commit.subject = commit.subject.replace(/\B@([a-z0-9](?:-?[a-z0-9/]){0,38})/g, replaceUserUrls)
            }

            // Customization: remove duplicate issue references and group by action
            commit.references = filterDuplicateReferences(commit);
            commit.referencesByAction = {};

            commit.references.forEach(reference => {
                const action = reference.action || 'relates to';
                if (!commit.referencesByAction[action]) {
                    commit.referencesByAction[action] = [];
                }

                commit.referencesByAction[action].push(reference)
            })

            return commit
        },
        groupBy: 'type',
        commitsSort: ['scope', 'subject'],
        noteGroupsSort: 'title',
        notesSort: compareFunc
    }
}

function filterDuplicateReferences(commit) {
    const referencesByIssue = {};

    commit.references.forEach(reference => {
        referencesByIssue[reference.issue] = reference;
    })

    return Object.values(referencesByIssue)
}

/**
 * Merge user set configuration with default configuration.
 */
function defaultConfig(config) {
    config = config || {}
    config.types = config.types || []
    config.issueUrlFormat = config.issueUrlFormat ||
        '{{host}}/{{owner}}/{{repository}}/issues/{{id}}'
    config.commitUrlFormat = config.commitUrlFormat ||
        '{{host}}/{{owner}}/{{repository}}/commit/{{hash}}'
    config.compareUrlFormat = config.compareUrlFormat ||
        '{{host}}/{{owner}}/{{repository}}/compare/{{previousTag}}...{{currentTag}}'
    config.userUrlFormat = config.userUrlFormat ||
        '{{host}}/{{user}}'
    config.issuePrefixes = config.issuePrefixes || ['#']

    return config
}

/**
 * Expand on the simple mustache-style templates supported in
 * configuration.
 */
function expandTemplate(template, context) {
    let expanded = template
    Object.keys(context).forEach(key => {
        expanded = expanded.replace(new RegExp(`{{${key}}}`, 'g'), context[key])
    })
    return expanded
}

/**
 * Find what kind of type the commit is.
 */
function findTypeEntry(types, commit) {
    const typeKey = (commit.revert ? 'revert' : (commit.type || '')).toLowerCase()

    return types.find((entry) => {
        if (entry.type !== typeKey) {
            return false
        }

        if (entry.scope && entry.scope !== commit.scope) {
            return false
        }

        return true
    })
}
