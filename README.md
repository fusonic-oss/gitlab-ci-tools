# Gitlab CI Tools
This is a collection of different tools related to Gitlab CI tasks.

- [Cleanup environments](docs/cleanup_environments.md)
- [Cleanup test databases](docs/cleanup_testdatabases.md)
- [Cleanup AWS S3](docs/cleanup_aws_s3.md)
- [Cleanup AWS SNS](docs/cleanup_aws_sns.md)
- [Cleanup AWS SNS Subscriptions](docs/cleanup_aws_sns_subscriptions.md)
- [Cleanup AWS SQS](docs/cleanup_aws_sqs.md)
- [.NET test execution](docs/dotnet_test.md)
- [Release Tool](docs/release_tool.md)
