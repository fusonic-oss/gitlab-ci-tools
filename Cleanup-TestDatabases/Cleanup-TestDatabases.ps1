#!/usr/bin/env pwsh

<#
.SYNOPSIS
Cleanup-TestDatabases.ps1
A utility script to remove unused test databases from a DB server. The test databases without
a corresponding branch in the repository will be removed. The test databases are expected to
use the CI_COMMIT_REF_SLUG for the name (for example ProjectTest_$CI_COMMIT_REF_SLUG)
.PARAMETER AccessToken
The gitlab API access token. This is normally the token of an own API-User created for that project.
.PARAMETER ProjectId
The gitlab ID of the project that should be cleaned.
.PARAMETER ConnectionString
The connection string to the database.
.PARAMETER TestDbPrefix
The prefix for the test database names.
.PARAMETER Exclude
Additonal databases to exclude from dropping. Example: If you set the parameter to "Template Test" and the prefix is "ProjectTest_", databases starting with ProjectTest_Template and ProjectTest_Test will not be dropped.
.PARAMETER DryRun
Don't drop the databases. Only outputs the command that would get executed.
.EXAMPLE
Cleanup-Environments.ps1 -AccessToken Abcd234fgh!jklmnop532 -ProjectId 1234567 -ConnectionString "Host=localhost;Database=postgres;User=hallo;Password=du" -TestDbPrefix ProjectTest_ -Exclude Template
#>

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !! If you change anything here, tag it and also update the tags !!
# !! used in cleanup_testdatabases.yml and cleanup_testdatabases.md !!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

param (
    [Parameter(Mandatory=$true)][string]$AccessToken,
    [Parameter(Mandatory=$true)][int]$ProjectId,
    [Parameter(Mandatory=$true)][string]$ConnectionString,
    [Parameter(Mandatory=$true)][string]$TestDbPrefix,
    [switch]$DryRun,
    [string[]]$Exclude
)

. ../Shared/Utils.ps1

$ErrorActionPreference = "Stop"

if ($DryRun)
{
    Write-Output "Dry run. Changes won't be applied."
}

#get current branches in repository
$branches = (Get-Branches -AccessToken $AccessToken -ProjectIds $ProjectId).name
if ($branches.Count -eq 0) {
    Write-Error "No branches available"
    exit 1
}

Write-Output "Found $($branches.Count) branches:" $branches

$branches = $branches.foreach{Get-Slug $PSItem}

$dryRunParam = "";
if ($DryRun)
{
    $dryRunParam = "--dryrun"
}

pgtestutil cleanup $dryRunParam -c $ConnectionString -p $TestDbPrefix -e (,$branches) (,$Exclude)
if ($LastExitCode -ne 0) { throw "pgtestutil exited with code $LastExitCode." }

Write-Output "Done"